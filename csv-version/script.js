// document.querySelectorAll('li').addEventListener("click", function(){
//     this.getElementByClassName('description').style.display = "block";
// });

let listItems = document.querySelectorAll("li");

listItems.forEach(function(elem) {
    elem.addEventListener("click", function() {
        this.querySelector('.description').classList.toggle("blockstyle");
    });
});