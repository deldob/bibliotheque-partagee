<!-- 
    This is a test document to attempt to do session things with PHP that i already do with flask
    Since PHP is older and used in more places, knowing its logic is helpful.
    THis program just asks you for two books and outputs a list of people who've done the same.

    Dobody
 --> 

<?php // Initial operations

// Function to return to login
function gotologin() {
    header('Location: index.php');
}

// Start the session
session_start();

// If no username set, go to login
if ( !isset($_SESSION["username"]) ) {
    gotologin();
}

// If there is not yet an array of books, create one
if(!isset($_SESSION["books"])){
    $_SESSION["books"] = array();
}

// If the submit input for resetting the session was pressed, unset the session variable with books 
if(isset($_POST["reset"])){
    unset($_SESSION["books"]);
}

// If logout button is pressed, unset the username and go to login
if(isset($_POST["logout"])){
    unset($_SESSION["username"]);
    gotologin();
}

$user = $_SESSION["username"];

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Books list</title>
        <style>
            
        </style>
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>

        <?php // Verification of POST method

            $hasinfo = False; // Initialise varaible and default to False to check if info is received in POST

            // pre_r($_POST); //Debug

            // If both first name and last name fields are empty, set $hasinfo to false. 
            // Else, there is info to add
            if ( isset($_POST["bookname"]) && isset($_POST["authorname"]) ) {
                if ( $_POST["bookname"] == "" && $_POST["authorname"] == "" ) {
                    $hasinfo = False;
                } else {
                    $hasinfo = True;
                }
            }

            echo "You are " . $user;
        ?>

        

        <form action="" method="post"> <!-- FOR BOOKS -->
            <p>Book Title: <input type="text" name="bookname" value=""></p>
            <p>Author: <input type="text" name="authorname" value=""></p>
            <p>Year: <input type="text" name="year"></p>
            <input type="submit" name="submit" value="Submit">
        </form>

        <br>

        <form action="" method="post"> <!-- FOR SESSION -->
            <input type="submit" name="reset" value="Reset entire list">
            <input type="submit" name="logout" value="Logout">
        </form>

        <?php 
            if($hasinfo){ // if $_POST is not an empty array
        
                // Here I am using classic PHP string concatenation
                echo "<br><p> You: " . $_SESSION["username"] . " added " . $_POST["bookname"] . " by " . $_POST["authorname"] . "</p>"; 

                // // since the post array's first 2 values are first and seocnd name, implode joins them with blank spaces
                // // it selects them by slicing the array and getting range from including index 0 to excluding index 2 
                // // (means including 2nd value)
                // echo "<br>Your name is ". implode(" ", array_slice($_POST, 0, 2)) . ". Hello!"; 
                // The session being initiated, we autogenerate the "books" array by adding the last POST request to it.
                array_push($_SESSION["books"], array_slice($_POST,0,2));

                // Add book to CSV
                $newbook = array_slice($_POST, 0, 3); // Title, Author, Year
                array_push($newbook, $user);
                $booksfile = 'books.csv';
                $books_table = fopen($booksfile, 'a');
                fputcsv($books_table, $newbook);
                fclose($books_table);

                // Only print out red warning if reset button was NOT pressed AND if submit button WAS pressed
            } elseif (!isset($_POST["reset"]) &&  isset($_POST["submit"])) {
                echo "<p style='color:red;'>You can't add an empty book</p>";
            }          
            unset($_POST);
        ?>

        <hr>

        <h3>Also added in this books table:</h3>

        <ul>
            <?php // List the books

                // Property corresponds to which index?
                $index_book_title = 0;
                $index_book_author = 1;
                $index_book_year = 2;
                $index_book_owner = 3;
                $index_book_lendee = 4;
                $index_book_ISBN = 5;
                $index_book_image = 6;
                $index_book_description = 7; // Description is last because longest 

                // read each line in CSV file at a time and store to $data
                $readbooksfile = 'books.csv';
                $books_table = fopen($readbooksfile,'r');
                while (($row = fgetcsv($books_table)) !== false) {
                    $data[] = $row;
                }
                $_SESSION["books"] = $data;
                fclose($books_table);

                // we put this in a handy variable. We can use it later if we want.
                $books = $_SESSION["books"];

                // For each value in the books array, represent it as $book_info and print it out 
                foreach(array_reverse($books) as $book_info){ 
                    echo "<li><span class='color'>" . $book_info[$index_book_title] . "</span> by <span class='color'>" . $book_info[$index_book_author] . "</span> written in <span class='color'>" . $book_info[$index_book_year] . "</span> added by <span class='color'>" . $book_info[$index_book_owner] . "</span><span class='description'>" . $book_info[$index_book_description] . "</span></li>"; 
                }

                // LET ME EXPLAIN THE LOGIC
                // $_SESSION is a dictionary, containing in its first position an array we will call $books.
                // $books is an array filled with arrays, each of which we will call $book_info when looping through
                // each $book_info is an array containing the values taken from a previous $_POST
                // Those values are the first and last books of previous people.
                // -----
                // HOWEVER those can be blank, and when they are, we must create a check to see if $_POST 
                // Contains only empty values

            ?>
        </ul>
        <script src='script.js'>

        </script>
    </body>
</html>

<?php // Functions definition

function pre_r( $array ) {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}



?>