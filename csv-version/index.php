<!-- 
    This is a test document to attempt to do session things with PHP that i already do with flask
    Since PHP is older and used in more places, knowing its logic is helpful.
    THis program just asks you for two names and outputs a list of people who've done the same.

    Dobody
 -->

<?php

// Function to Redirect user to dashboard page
function dashboard() {
    header('Location: dashboard.php');
}

// Start the session
session_start();

// If there is not yet an array of books, create one
if(!isset($_SESSION["books"])){
    $_SESSION["books"] = array();
}

// If username not set, assign from results, if it is set, go to dashboard
if( !isset($_SESSION["username"]) ) {
    if(!empty($_POST["username"])){
        // assign the username to the session var
        $user = $_POST["username"];
        $_SESSION["username"] = $user;
        dashboard();
    }
} else {
    dashboard();
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Book list</title>
        <style>
            
        </style>
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>


        <h1>Login Page</h1>
        <p>Please choose a username:</p><br>

        <form action="" method="post">
            <p>Username: <input type="text" name="username" value=""></p>
            <input type="submit" name="submit" value="Login">
        </form>
        <br>

        <?php
            echo "You are " . $_SESSION["username"]; // Debug to see if session variable is correct

        ?>

        <?php
            echo "<hr>"; // Readability

            // we put said array in handy variable
            $books = $_SESSION["books"];

            echo "<h3>Also added in this session:</h3>";

            // For each value in the books array, represent it as $name 
            // and print it out to show everyone who's beein in session
            foreach(array_reverse($books) as $name){
                
                    echo "<br>" . implode(" by ", $name); 
                
            }
        
        ?>

    </body>
</html>

<?php

function pre_r( $array ) {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

?>